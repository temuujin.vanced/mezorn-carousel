import React, { useState, useEffect } from "react";
import axios from "axios";
import { Carousel } from "./components/Carousel";
import useDelayUnmount from "./hooks/useDelayUnmount";
import { Controls } from "./components/Controls";
import { Collage } from "./components/Collage";
import PlusIcon from "mdi-react/PlusIcon";
import MinusIcon from "mdi-react/MinusIcon";

const axiosInstance = axios.create({
  baseURL: "https://dummyapi.io/data/v1/user",
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
    "app-id": "62a9b0501f5984d08960fca3",
  },
});

function App() {
  const [images, setImages] = useState([]);
  const [tmpImages, setTmpImages] = useState([]);
  const [imageCount, setImageCount] = useState(3);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [isMounted, setIsMounted] = useState(false);
  const [isIdle, setIsIdle] = useState(false);

  const showSlider = useDelayUnmount(isMounted, 600);

  const toggleSlide = (type, index) => {
    switch (type) {
      case "close":
        setIsMounted(false);
        break;
      case "show":
        setIsMounted(true);
        changeSlide(index);
        break;
      default:
        setIsMounted(!isMounted);
        break;
    }
  };
  const changeSlide = (index) => {
    var preview = document.querySelector(`#preview-slide-${index}`);
    if (
      (currentIndex == 0 && index > currentIndex) ||
      (index >= 0 && index <= tmpImages.length - 1) ||
      (currentIndex == tmpImages.length - 1 && index < currentIndex)
    ) {
      setCurrentIndex(index);
      if (!isIdle) {
        preview.scrollIntoView({ inline: "center" });
      }
    }
  };

  const handleKeyPress = (key) => {
    key.preventDefault();
    switch (key.code) {
      case "ArrowLeft":
        changeSlide(currentIndex - 1);
        break;
      case "ArrowRight":
        changeSlide(currentIndex + 1);
        break;
      case "Escape":
        toggleSlide("close");
        break;
      default:
        break;
    }
  };

  useEffect(() => {
    axiosInstance
      .get("https://dummyapi.io/data/v1/post", {
        limit: 5,
      })
      .then((response) => {
        setImages(response.data.data);
        setTmpImages(response.data.data.slice(0, imageCount));
      });
  }, []);
  return (
    <div
      tabIndex={0}
      onKeyDown={handleKeyPress}
      className={` bg-gray-50 overflow-clip w-screen h-screen relative text-white flex items-center justify-center p-5`}
    >
      <Collage
        items={tmpImages}
        handleClick={(index) => {
          toggleSlide("show", index);
        }}
      ></Collage>

      <Carousel
        items={tmpImages}
        currentIndex={currentIndex}
        showSlider={showSlider}
        isMounted={isMounted}
        changeSlide={changeSlide}
        toggleSlide={toggleSlide}
      ></Carousel>
      <Controls
        images={tmpImages}
        currentIndex={currentIndex}
        changeSlide={changeSlide}
        closeSlide={toggleSlide}
        showSlider={showSlider}
        isMounted={isMounted}
        isIdle={isIdle}
        setIsIdle={setIsIdle}
      ></Controls>

      <div className="z-0 absolute bottom-5 w-full pt-5 flex flex-row justify-center space-x-10 items-center">
        <div
          onClick={() => {
            if (imageCount > 1) {
              setTmpImages(images.slice(0, imageCount - 1));
              setImageCount(imageCount - 1);
            }
          }}
          className="p-1 cursor-pointer rounded-md bg-gray-600"
        >
          <MinusIcon className="fill-current"></MinusIcon>
        </div>
        <div className="text-black font-bold text-xs select-none">
          Зураг нэмэх/хасах
        </div>
        <div
          onClick={() => {
            if (imageCount < images.length) {
              setTmpImages(images.slice(0, imageCount + 1));
              setImageCount(imageCount + 1);
            }
          }}
          className="p-1 cursor-pointer rounded-md bg-gray-600"
        >
          <PlusIcon className="fill-current"></PlusIcon>
        </div>
      </div>
    </div>
  );
}

export default App;
