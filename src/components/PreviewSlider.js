export const PreviewSlider = ({ items, activeIndex, handleClick }) => {
  return (
    <div className="flex items-center justify-center">
      <div className="h-16 rounded-xl backdrop-blur-xl bg-black/30 max-w-2xl flex flex-nowrap items-center no-scrollbar scroll-smooth overflow-scroll px-3 space-x-3">
        {items.map((i, index) => {
          return (
            <img
              id={`preview-slide-${index}`}
              onClick={() => {
                handleClick(index);
              }}
              src={i.image}
              key={index}
              className={`cursor-pointer flex-shrink-0 w-10 h-10 rounded-md object-cover transition duration-300 border-2 border-purple-400 ${
                activeIndex == index ? `border-opacity-100` : `border-opacity-0`
              }`}
            />
          );
        })}
      </div>
    </div>
  );
};
