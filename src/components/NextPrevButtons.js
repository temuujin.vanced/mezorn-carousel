import ArrowBackIcon from "mdi-react/ArrowBackIcon";
import ArrowForwardIcon from "mdi-react/ArrowForwardIcon";

export const NextPrevButtons = ({ handleClick, currentIndex, length }) => {
  return (
    <div className="flex flex-row justify-between">
      {currentIndex > 0 ? (
        <div
          onClick={() => {
            handleClick(currentIndex - 1);
          }}
          className="cursor-pointer backdrop-blur-xl bg-black/30 rounded-full h-10 sm:h-16 w-10 sm:w-16 flex items-center justify-center"
        >
          <ArrowBackIcon className="fill-current" />
        </div>
      ) : (
        <div></div>
      )}
      {currentIndex == length - 1 ? (
        <div></div>
      ) : (
        <div
          onClick={() => {
            handleClick(currentIndex + 1);
          }}
          className="cursor-pointer backdrop-blur-xl bg-black/30 rounded-full h-10 sm:h-16 w-10 sm:w-16 flex items-center justify-center"
        >
          <ArrowForwardIcon className="fill-current" />
        </div>
      )}
    </div>
  );
};
