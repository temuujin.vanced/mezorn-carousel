import { CarouselItem } from "./CarouselItem";

export const Carousel = ({ items, currentIndex, isMounted, showSlider }) => {
  return (
    <div
      className={`z-0 opacity-0 absolute inset-0 carousel bg-black bg-opacity-90 flex overflow-hidden items-center transition transform duration-300 ease-in-out ${
        showSlider && isMounted && "!opacity-100 !z-20"
      }`}
    >
      {(showSlider || isMounted) && (
        <div
          className="flex flex-nowrap transition duration-300 ease-in-out z-0"
          style={{
            transform: `translateX(-${currentIndex * (100 / items.length)}%)`,
          }}
        >
          {items.map((i, index) => {
            return (
              <CarouselItem key={index} slide={i} index={index}></CarouselItem>
            );
          })}
        </div>
      )}
    </div>
  );
};
