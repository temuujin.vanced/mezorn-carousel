import { PreviewSlider } from "./PreviewSlider";
import { SlideIndicator } from "./SlideIndicator";
import { CloseButton } from "./CloseButton";
import { NextPrevButtons } from "./NextPrevButtons";
import React, { useState } from "react";
import useInterval from "../hooks/useInterval";

export const Controls = ({
  currentIndex,
  images,
  changeSlide,
  closeSlide,
  showSlider,
  isMounted,
  isIdle,
  setIsIdle,
}) => {
  const [mouseMoved, setMouseMoved] = useState(true);

  useInterval(() => {
    if (!mouseMoved) {
      setIsIdle(true);
    }
    setMouseMoved(false);
  }, 3000);
  return (
    <div
      onMouseMove={(e) => {
        setMouseMoved(true);
        setIsIdle(false);
      }}
      className={`z-0 overflow-clip transition duration-300 ease-in-out absolute inset-0 p-1 sm:p-5 pb-1 ${
        showSlider && "!z-20"
      }`}
    >
      <div
        className={`w-full h-full scale-150 opacity-0 origin-center transition duration-300 ease-in-out flex flex-col justify-between ${
          showSlider && isMounted && !isIdle && "!scale-100 !opacity-100"
        }`}
      >
        <div className="flex flex-row justify-between">
          <SlideIndicator
            current={currentIndex + 1}
            length={images.length}
          ></SlideIndicator>
          <CloseButton
            handleClick={() => {
              closeSlide();
            }}
          ></CloseButton>
        </div>
        <NextPrevButtons
          handleClick={changeSlide}
          currentIndex={currentIndex}
          length={images.length}
        ></NextPrevButtons>
        <div className="flex justify-center items-center">
          <PreviewSlider
            items={images}
            activeIndex={currentIndex}
            handleClick={changeSlide}
          ></PreviewSlider>
        </div>
      </div>
    </div>
  );
};
