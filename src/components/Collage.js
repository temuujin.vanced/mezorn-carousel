export const Collage = ({ items, handleClick }) => {
  const style = (index) => {
    let tmp = null;

    switch (items.length) {
      case 1:
        tmp = "col-span-6 row-span-6 ";
        break;
      case 2:
        tmp = "col-span-3 row-span-6";
        break;
      case 3:
        if (index == 0) {
          tmp = "col-span-6 row-span-4";
        } else {
          tmp = "col-span-3 row-span-2";
        }
        break;
      case 4:
        if (index == 0) {
          tmp = "col-span-4 row-span-6";
        } else {
          tmp = "col-span-2 row-span-2";
        }
        break;
      default:
        if (index < 2) {
          tmp = "col-span-3 row-span-3";
        } else if (index + 1 == 5) {
          tmp = "col-span-3 row-span-2 relative";
        } else {
          tmp = "col-span-3 row-span-2";
        }
    }

    return tmp;
  };

  return (
    <div
      className={`z-10 w-4/5 sm:w-1/2 md:w-80 h-auto aspect-square rounded-md grid grid-cols-6 grid-rows-6 ${
        items.length < 5 ? "grid-flow-dense" : "grid-flow-col"
      } gap-1`}
    >
      {items.slice(0, 5).map((i, index) => {
        return (
          <div key={index} className={style(index)}>
            <img
              onClick={() => {
                handleClick(index);
              }}
              key={index}
              src={i.image}
              className="cursor-pointer w-full h-full object-cover"
            />

            {items.length > 5 && index + 1 == 5 && (
              <div
                onClick={() => {
                  handleClick(index);
                }}
                className="absolute select-none cursor-pointer inset-0  bg-black bg-opacity-70 flex items-center justify-center font-black"
              >
                +{items.slice(5, items.length).length}
              </div>
            )}
          </div>
        );
      })}
    </div>
  );
};
