export const CarouselItem = ({ slide, index }) => {
  return (
    <div
      key={index}
      className="flex-shrink-0 w-screen h-screen inline-flex items-center justify-center"
    >
      <img
        id={`carousel-slide-${index}`}
        src={slide.image}
        className="object-contain h-full w-full"
      />
    </div>
  );
};
