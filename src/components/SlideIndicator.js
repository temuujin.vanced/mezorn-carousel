export const SlideIndicator = ({ current, length }) => {
  return (
    <div className="backdrop-blur-xl bg-black/30 rounded-full h-10 sm:h-16 w-20 sm:w-28 flex items-center justify-center font-bold">
      {current}/{length}
    </div>
  );
};
