import CloseIcon from "mdi-react/CloseIcon";

export const CloseButton = ({ handleClick }) => {
  return (
    <div
      onClick={handleClick}
      className="cursor-pointer backdrop-blur-xl bg-black/30 rounded-full h-10 sm:h-16 w-10 sm:w-16 flex items-center justify-center"
    >
      <CloseIcon className="fill-current" />
    </div>
  );
};
